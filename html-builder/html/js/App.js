require({
    waitSeconds: 15,
    baseUrl: '',
    //TODO: change to scriptsmin once the minimizer is working on the build server
    paths: {
        // jquery: '//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min',
        jquery: 'http://code.jquery.com/jquery-3.2.1.min',
        // jqueryui: 'libs/jquery-ui.min',
        ko: 'libs/knockout-3.4.2',
        bootstrap: 'libs/bootstrap/bootstrap'
    },
    priority: ['jquery'],
    shim: {
        bootstrap: {
            deps: ["jquery"]
        }
        /*,mjqueryui: {
            deps: ["jquery"]
        }*/
    }
});

require([
        "jquery",
        "js/drag/drag",
        "js/elements-ui/elements",
        "js/builder-ui/sidenav",
        "js/builder-ui/bootstrap-ui",        
        "js/model/model-html",        
        "bootstrap",
        // "depend![jquery,jqueryui]",
        "libs/depend![jquery]",
        "libs/domReady!"
        ],
    function ($, drag, elements, sidenav, bootstrapui, modelHtml) {

        // console.log(drag);
        drag.init();
        elements.init();
        bootstrapui.init();
        

    });


