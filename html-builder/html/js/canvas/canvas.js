define(
	[
        'jquery',
        // ,'jqueryui'
    ],
    function ($){

		var self = {};
		self.canvas = document.querySelector('#canvas');
		self.ctx = canvas.getContext("2d");
		self.ctx.width = $(window).width();
		self.ctx.height = $(window).height();

		self.drawLine = function(e, mouseDownX, mouseDownY){
			// console.log( 'drawLine, mouseDownX: ' + mouseDownX + ', mouseDownY: ' + mouseDownY );
			console.log( e );
			// console.log( 'self.ctx.width: ' + self.ctx.width + ", self.ctx.height: " + self.ctx.height );

			self.clear();
			self.drawCircle(mouseDownX, mouseDownY);

			self.ctx.beginPath();
			self.ctx.lineWidth = 1;
			self.ctx.moveTo(mouseDownX,mouseDownY);
			self.ctx.lineTo(e.x, e.y);

			

			// self.ctx.lineTo(e.screenX, e.screenY);
			// self.ctx.moveTo(0,0);
			// self.ctx.lineTo(100, 100);
			self.ctx.stroke();
		}
		self.drawCircle = function(mouseDownX, mouseDownY){
			self.ctx.beginPath();
			self.ctx.arc(mouseDownX, mouseDownY, 15, 0, 2 * Math.PI, false);
			self.ctx.fillStyle = 'rgba(255, 0, 0, .3)';
			self.ctx.fill();
		    self.ctx.lineWidth = 1;
		    self.ctx.strokeStyle = '#ff0000';
		    self.ctx.stroke();
		    
		    // self.ctx.endFill();
		}
		self.clear = function(){
			self.ctx.clearRect(0, 0, self.ctx.width, self.ctx.height);

		}

        return self;
    }
);