define(
	[
        'jquery',
        'js/elements-ui/elements',
        'js/canvas/canvas',
        'js/core/controller-ui',
        // ,'jqueryui'
    ],
    function ($, elements, canvas, controllerUI){
        
        

        /*
        *
		* Divs - mouseover, highlight
		*
		*/
		var self = {};

		self.id = 0;
		self.uniqueId = function( uiName ){
			self.id++;
			return uiName + "_" + self.id;
		}

		self.dropDiv = $.parseHTML( '<div class="drop-div"></div>' );
		self.uiMouseOverDiv = $.parseHTML( '<div class="ui-mouseover-div"><a><i class="fa fa-arrows"></i></a><a><i class="fa fa-edit"></i></a></div>' );
		self.highlightDiv = $.parseHTML( '<div class="ui-highlight-div"></div>' );

		$(self.uiMouseOverDiv).children('a').on('click', function(e){
			alert(1);
		});
		/*$(self.uiMouseOverDiv).on('mouseover', function(e){
			// console.log('uiMouseOverDiv - mouseover');
			// e.stopPropagation();
			// e.stopImmediatePropagation();
		});

		$(self.uiMouseOverDiv).on('mouseout', function(e){
			// console.log('uiMouseOverDiv - mouseover');
			// e.stopPropagation();
			// e.stopImmediatePropagation();
		});*/

		
		/*
        *
		* Drag & Drop
		*
		*/
		self.currentDragable;


		$( "[dropable]" ).each(function() {
			$(this).on('drop', function(evt) {
				// console.log('---- drop ------');
				self.drop( evt.originalEvent );
			});
			$(this).on('dragover', function(evt) {
				// console.log('---- dragover ------');
				self.allowDrop( evt.originalEvent );
			});

		});


		self.bindUIMenuDragstart = function(elem){
        	$(elem).on('dragstart', function(evt) {
			   //create element make it positioning absolute
			   var uiName = $(this).attr('ui-addable');
			   var ui = $.parseHTML( controllerUI.addUI( uiName ) );
			   var id = self.uniqueId( uiName );
			   $(ui).attr('id', id );

			   self.currentDragable = ui;

			   // console.log( evt.originalEvent.dataTransfer );			   
			   evt.originalEvent.dataTransfer.setData( 'id' , id );
			});
        }
        $( "[ui-addable]" ).each(function() {
          	self.bindUIMenuDragstart(this);          	
        });

		self.allowDrop = function (e) {
			// console.warn( 'self.allowDrop' );
		    e.preventDefault();
		    if( $(e.target).is("[dropable]") ){
			    $(".droppable").removeClass('droppable');
			    $(e.target).addClass('droppable');

			    $(self.dropDiv).detach();
			    $(e.target).prepend(self.dropDiv);
			}

		    // console.log($(e.target));

		}

		self.bind_AddedUI_Dragstart = function(elem){
        	$(elem).off('dragstart');
        	$(elem).on('dragstart', function(evt) {
        		var id = $(elem).attr('id');
        		console.log('bind_AddedUI_Dragstart - dragstart, ID: ' + id );
        		self.currentDragable = $(elem);
			   	evt.originalEvent.dataTransfer.setData( 'id' , id );
			});
        }

		self.drag = function (e) {
			console.warn( 'self.drag' );
		    e.dataTransfer.setData("id", e.target.id);
		}

		self.drop = function (e) {

			$(self.dropDiv).detach();
			if( $(e.target).is("[dropable]") ){

				console.warn( 'self.drop' );
			    e.preventDefault();
			    $(".droppable").removeClass('droppable');
			    var data = e.dataTransfer.getData("id");
			    console.log('data: ');
			    console.log(data);
			    console.log(e.target);
			    $(self.currentDragable).detach();
			    $(e.target).append( self.currentDragable );

			    self.bind_AddedUI_Dragstart( $(self.currentDragable) );
			    self.bindMouseOverOut( $(self.currentDragable) );
			    console.log("self.currentDragable");
			    console.log(self.currentDragable);
			    self.currentDragable = null;
		    }
		    else if($(e.target).parent().is("[dropable]"))
		    {
		    	console.log("++++ Parent dropable");
		    }
			else
		    {
		    	console.log("!!!!! cant drop");
		    }

		    // e.target.appendChild(document.getElementById(data));
		}



		self.stopPropagation = function(e){
			e.stopPropagation();
		}
		self.stopImmediatePropagation = function(e){
			e.stopImmediatePropagation();
		}

		self.bindMouseOverOut = function(elem){
			// console.log('bindMouseOverOut');
			console.log(elem);
			// elem.removeEventListener('mouseover', self.mouseover);
			// elem.children('*').off('mouseover');
			elem.addEventListener('mouseover', self.mouseover, false);
			// elem.addEventListener('mouseover', self.mouseover, true);
			
			let children = elem.querySelectorAll('*:not(.selectable)');

			children.forEach(function(el) {
				el.addEventListener('mouseover', self.stopPropagation, false);
				el.addEventListener('mouseout', self.stopPropagation, false);
			})

			// elem.removeEventListener('mouseout', self.mouseout);
			// elem.children('*').off('mouseout');
			elem.addEventListener('mouseout', self.mouseout, false);

		}
		
		self.lastMouseOver;

		self.mouseover = function(e){
			// e.stopPropagation();

			// console.warn('mouseover');
			// console.log("target: ", e.target);
			// console.log("relatedTarget: ", e.relatedTarget);
			// console.log("lastMouseOver: ", self.lastMouseOver);

			// if ( !self.lastMouseOver || !self.isInside( e.target, self.lastMouseOver ) ){
				//get x y offset
				
				// console.error('mouseover');

				$(self.uiMouseOverDiv).css('opacity',0.1);
				$(self.highlightDiv).css('opacity',0.1);

				//append mouseover divs only to body as it wont append to img
				$( 'body' ).append( self.uiMouseOverDiv );
				$( 'body' ).append( self.highlightDiv );

				$(self.uiMouseOverDiv).animate({
				    opacity: 1
				}, 150);

				$(self.highlightDiv).animate({
				    opacity: 1
				}, 150);

				$( self.uiMouseOverDiv ).css({
					/*'left': 0,
					'top': 0*/
					/*'top': - $( self.uiMouseOverDiv ).outerHeight()*/
					'left': $(e.target).offset().left + 3,
					'top': $(e.target).offset().top + 3
				});
				$( self.highlightDiv ).css({
					'left': $(e.target).offset().left,
					'top': $(e.target).offset().top,
					'width': $(e.target).outerWidth(), 
					'height': $(e.target).outerHeight()
				});

				self.lastMouseOver = e.target;
			// }
			
		}
		self.mouseout = function(e){
			// e.stopPropagation();

			// console.warn('mouseout');
			// console.log("target: ", e.target);
			// console.log("relatedTarget: ", e.relatedTarget);

			// console.log('----------- mouseout ----------');
			// console.log('$(e.relatedTarget): ');
			// console.log($(e.relatedTarget));
			// console.log('$(e.target): ');
			// console.log($(e.target));
			// console.log('    $(self.uiMouseOverDiv): ');
			// console.log($(self.uiMouseOverDiv));
			// console.log('---------------------------------');

			//check if mouse was on any of the children of Mouseover div
			if( $( $(e.relatedTarget)[0] ).closest(".ui-mouseover-div").length > 0 ){
				// console.log("uiMouseOverDiv children - relatedTarget")
				return;
			}

			//check if mouse was on Mouseover div
			if( $(e.relatedTarget) === $(self.uiMouseOverDiv)[0] ){
				// console.log("uiMouseOverDiv - relatedTarget")
				return;
			}

			if (!self.isInside( e.relatedTarget, self.lastMouseOver ) ){
				
				// console.error('mouseout');

				$( self.uiMouseOverDiv ).detach();
				$( self.highlightDiv ).detach();

				self.lastMouseOver = null;
			}
		}

		//bind mouseover
		// self.bindMouseOverOut( document.querySelector('#preview > .container > .div-text-img') );
		
		//bind mouseover
		let uiMouseOverDiv = $(self.uiMouseOverDiv)[0];
		// console.log(uiMouseOverDiv);
		uiMouseOverDiv.addEventListener('mouseout', self.mouseout, false);


		// console.log( "document.querySelector" );
		// console.log( document.querySelector('#preview > .container > .div-text-img') );


		self.isInside = function(node, target) {
			// console.log("------- isInside ------");
			// console.log("node: ", node);
			// console.log("lastMouseOver: ", target);
			if(node == target)
				node = node.parentNode;
			var i = 0;
			for (; node != null; node = node.parentNode){
				i++;
				// console.log( node );
				if (node == target){
					// console.log('----------------- isInside', i);
					return true;
				}
			}
		}
		
		window.onkeyup = function(e) {
		   // console.log(e.key);
		   if(e.key == 'c')
		   		console.clear();
		}

		

		self.isMouseDown;
		self.mouseDownX;
		self.mouseDownY;

		self.mouseUp = function(e){
			self.isMouseDown = false;
			canvas.clear();
			document.removeEventListener('mousemove', self.mouseMove, false);
		}
		self.mouseDown = function(e){
			canvas.drawLine(e);
			self.isMouseDown = true;
			// self.mouseDownX = e.screenX;
			// self.mouseDownY = e.screenY;
			self.mouseDownX = e.x;
			self.mouseDownY = e.y;

			canvas.drawCircle(self.mouseDownX, self.mouseDownY);

			console.log('---- mouseDown');
			console.log('self.mouseDownX: ' + self.mouseDownX);
			console.log('self.mouseDownY: ' + self.mouseDownY);

			document.addEventListener('mousemove', self.mouseMove, false);
		}
		self.mouseMove = function(e){
			// console.log('++ mouseMove');
			if(self.isMouseDown){
				canvas.drawLine(e, self.mouseDownX, self.mouseDownY);
				self.isMouseDown = true;
			}
		}
		$('#canvas').attr('width', $(window).width() );
		$('#canvas').attr('height', $(window).height() );

		document.addEventListener('mousedown', self.mouseDown, false);
		document.addEventListener('mouseup', self.mouseUp, false);
		

        self.init = function(){

        }

        return self;
    }
);