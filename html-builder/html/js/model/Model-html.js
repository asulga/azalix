define(
	[
        'jquery',
        'ko',
        'js/elements-ui/elements',
        'js/drag/drag'
        // ,'jqueryui'
    ],
    function ($, ko, elements, drag){

    	var self = {};

		self.addUI = function( uiName ){

		}

        /*function MyViewModel() {
            this.buyer = { name: 'Franklin', credits: 250 };
            this.seller = { name: 'Mario', credits: 5800 };
        }
        ko.applyBindings(new MyViewModel());*/
		
        var template = 'templates.html';
        var targetID = 'templatesContainer';
        var partialVM = {data : 1};

        var load = function (template, targetID, partialVM) {
            console.log('load template');
            $.ajax({
                    url: template,
                    type: "GET",
                    crossDomain: true,
                    async: true,
                    success: function (stream) {
                            console.log('success');
                            console.log(stream);
                            $('#' + targetID).html( stream );
                            // alert($('#' + targetID));
                            ko.applyBindings(partialVM, document.getElementById("preview"));
                            // alert('loaded');
                        }
                });
        };

        /*
        * Main View Model
        */
        var viewModel = {
            employees: ko.observableArray([
                { name: "jumbotron", textName: "jumbotron 1", active: ko.observable(true) },
                { name: "jumbotron", textName: "jumbotron 2", active: ko.observable(false) },
                { name: "div-text-img", textName: "div-text-img 1", active: ko.observable(false) }
            ]),
            displayMode: function(employee) {
                // Initially "Kari" uses the "active" template, while the others use "inactive"
                return employee.active() ? "active" : "inactive";
            },
            getName: function(employee) {
                return employee.name;
            }
        };

        viewModel.afterRender = function(elements) {
            // "elements" is an array of DOM nodes just rendered by the template
            // You can add custom post-processing logic here
            console.log('afterRender');
            console.log(elements);

            drag.bindMouseOverOut( elements[1] );
            // console.log( drag.bindMouseOverOut );
        }

        /*
        * Load HTML templates
        */
        load( 'templates.html', 'templatesContainer', viewModel );
     
        
        



        return self;
    }
);