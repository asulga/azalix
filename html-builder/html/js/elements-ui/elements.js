define(
	[
        'jquery',
        'bootstrap'
    ],
    function ($) 
    {
        var self = {};
        
        self.ui = {
            'Jumbotron': '<div class="jumbotron" draggable="true">'+
                            '<h1>Jumbotron</h1>'+
                            '<p>This is a template showcasing the optional theme stylesheet included in Bootstrap. Use it as a starting point to create something more unique by building on or modifying it.</p>'+
                         '</div>',
            'TextImg':   '<div class="div-text-img" draggable="true"><h3 class="selectable">Text and Image</h3><img src="images/Apple-Watch-logo-600.png" class="img-responsive" /></div>' ,
            'Container': '<div class="container" draggable="true" dropable></div>'
        };

        self.init = function () {
            console.log('elements.init');

            $( "[ui-addable]" ).each(function() {
              // console.log( $(this).attr('ui-addable') );

              $(this).attr('data-title', self.ui[$(this).attr('ui-addable')])
            });
        }

        $("#test-parent").on("mouseover", function(e){
            e.stopPropagation();
            console.log('mouseover - parent');
        });
        /*$("#test-parent").on("mouseout", function(e){
            e.originalEvent.preventDefault();
            console.log('mouseout - parent');
        });*/
        $("#test-child").on("mouseover", function(e){
            e.stopPropagation();
            $(".mouseover").removeClass("mouseover");
            $(e.target).addClass("mouseover");
            console.log( e.originalEvent );
            console.log('mouseover - child');
        });
        /*$("#test-child").on("mouseout", function(e){
            e.originalEvent.preventDefault();
            console.log('mouseout - child');
        });*/

        return self;
    }
);