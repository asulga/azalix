define(
	[
        'jquery',
        'bootstrap'
    ],
    function ($) 
    {
        var self = {};
        self.init = function () {
            // console.log('bootstrap-ui init');

            $("[data-toggle=tooltip]").tooltip({
                html: true,
                trigger: 'hover',
                container: 'body',
                // viewport: { "selector": "body", "padding": 15 }
            });

        }

        return self;

    }
);